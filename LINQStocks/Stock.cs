﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQStocks
{
    class Stock
    {
        public string TickerSymbol { get; set; }
        public string StockName { get; set; }
        public decimal SharePrice { get; set; }
        public decimal UnitsOwned { get; set; }

        public Stock(string tickerSymbol, string stockName, decimal sharePrice, decimal unitsOwned) 
        {
            this.TickerSymbol = tickerSymbol;
            this.StockName = stockName;
            this.SharePrice = sharePrice;
            this.UnitsOwned = unitsOwned;
        }

    }
}
