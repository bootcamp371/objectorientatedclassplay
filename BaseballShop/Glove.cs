﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballShop
{
    class Glove : InventoryItem
    {
        public string Type { get; set; }
        public string Color { get; set; }

        public Glove(string inventoryItem, string item, string manufacturer, decimal price, int quantity,
               string type, string color) : base(inventoryItem, item, manufacturer, price, quantity)
        {
            this.Type = type;
            this.Color = color;
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine($"{"Type:",13} " + this.Type);
            Console.WriteLine($"{"Color:",13} " + this.Color);
        }
        public override void CreateInvoice(StreamWriter outputFile, string name, string streetAddress)
        {
            base.CreateInvoice(outputFile, name, streetAddress);
            outputFile.WriteLine($"{"Type:",13} " + this.Type);
            outputFile.WriteLine($"{"Color:",13} " + this.Color);
        }
    }
}
