﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballShop
{
    class BattingGlove : InventoryItem
    {
        public string Size { get; set; }
        public string Durability { get; set; }

        public BattingGlove(string inventoryItem, string item, string manufacturer, decimal price, int quantity,
               string size, string durability) : base(inventoryItem, item, manufacturer, price, quantity)
        {
            this.Size = size;
            this.Durability = durability;
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine($"{"Size:",13} " + this.Size);
            Console.WriteLine($"{"Durability:",13} " + this.Durability);
        }

        public override void CreateInvoice(StreamWriter outputFile, string name, string streetAddress)
        {
            base.CreateInvoice(outputFile, name, streetAddress);
            outputFile.WriteLine($"{"Size:",13} " + this.Size);
            outputFile.WriteLine($"{"Durability:",13} " + this.Durability);
        }
    }
}
