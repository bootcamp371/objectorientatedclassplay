﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballShop
{
    public class Bat : InventoryItem
    {
        public string Name { get; set; }
        public int Length { get; set; }
        public int Weight { get; set; }

        public Bat(string inventoryItem, string item, string manufacturer, decimal price, int quantity,
               string name, int length, int weight) : base(inventoryItem, item, manufacturer, price, quantity)
        {
            this.Name = name;
            this.Length = length;
            this.Weight = weight;
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine($"{"Name:",13} " + this.Name);
            Console.WriteLine($"{"Length:",13} " + this.Length);
            Console.WriteLine($"{"Weight:",13} " + this.Weight);
            
        }
        public override void CreateInvoice(StreamWriter outputFile, string name, string streetAddress)
        {
            base.CreateInvoice(outputFile, name, streetAddress);
            outputFile.WriteLine($"{"Name:",13} " + this.Name);
            outputFile.WriteLine($"{"Length:",13} " + this.Length);
            outputFile.WriteLine($"{"Weight:",13} " + this.Weight);
        }
    }

    
}
