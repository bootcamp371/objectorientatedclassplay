﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballShop
{
    public class InventoryItem
    {
        public string InventoryNum { get; set; }
        public string Item { get; set; }
        public string Manufacturer { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public InventoryItem(string inventoryItem, string item, string manufacturer, decimal price, int quantity)
        {
            this.InventoryNum = inventoryItem;
            this.Item = item;
            this.Manufacturer = manufacturer;
            this.Price = price;
            this.Quantity = quantity;
        }

        public virtual void Display()
        {
            Console.WriteLine("");
            Console.WriteLine("Item Detail");
            Console.WriteLine("-----------");
            Console.WriteLine($"{"ID:",13} " + this.InventoryNum);
            Console.WriteLine($"{"Item:",13} " + this.Item);
            Console.WriteLine($"{"Manufacturer:",13} " + this.Manufacturer);
            Console.WriteLine($"{"Price:",13} " + this.Price);
            Console.WriteLine($"{"Qty On Hand:",13} " + this.Item);
        }
        public void DisplayAllInventory()
        {
            Console.WriteLine($"{this.InventoryNum,2}{this.Item,15}{this.Manufacturer,20}");
        }

        public virtual void CreateInvoice(StreamWriter outputFile, string name, string streetAddress)
        {
            outputFile.WriteLine("Congratulations on your Equipment Purchase!");
            outputFile.WriteLine("--------------------------------");
            outputFile.WriteLine($"{"Name:",13} " + name);
            outputFile.WriteLine($"{"Address:",13} " + streetAddress);
            outputFile.WriteLine($"{"Item:",13} " + this.Item);
            outputFile.WriteLine($"{"Manufacturer:",13} " + this.Manufacturer);
            outputFile.WriteLine($"{"Price:",13} " + this.Price);
        }
    }
}
