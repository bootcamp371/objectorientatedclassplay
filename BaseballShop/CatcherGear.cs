﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballShop
{
    class CatcherGear : InventoryItem
    {
        public string Size { get; set; }
        public string Color { get; set; }


        public CatcherGear(string inventoryItem, string item, string manufacturer, decimal price, int quantity,
               string size, string color) : base(inventoryItem, item, manufacturer, price, quantity)
        {
            this.Size = size;
            this.Color = color;
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine($"{"Size:",13} " + this.Size);
            Console.WriteLine($"{"Color:",13} " + this.Color);
        }
        public override void CreateInvoice(StreamWriter outputFile, string name, string streetAddress)
        {
            base.CreateInvoice(outputFile, name, streetAddress);
            outputFile.WriteLine($"{"Size:",13} " + this.Size);
            outputFile.WriteLine($"{"Color:",13} " + this.Color);
        }
    }
}
