﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BaseballShop
{
    class Program
    {
        static void Main(string[] args)
        {
            string selection;
            List<InventoryItem> inventoryList = new List<InventoryItem>();
            inventoryList = LoadData();

            do
            {
                Console.WriteLine("1 - Find Items that match Category");
                Console.WriteLine("2 - Find Items in a price range");
                Console.WriteLine("3 - Find Items by Manufacturer");
                Console.WriteLine("4 - Show all items");
                Console.WriteLine("5 - Add an Item");
                Console.WriteLine("Q - Quit");
                Console.Write("Enter in what you would like to do: ");
                selection = Console.ReadLine();

                if (selection == "1")
                {
                    SearchByCategory(inventoryList);
                }
                else
                if (selection == "2")
                {
                    SearchByPrice(inventoryList);

                }
                else
                if (selection == "3")
                {
                    SearchByManu(inventoryList);
                }
                else
                if (selection == "4")
                {
                    ShowAll(inventoryList);
                }
                else
                if (selection == "5")
                {
                    inventoryList = AddItem(inventoryList);
                }
                else
                if (selection != "Q")
                {
                    Console.WriteLine("Invalid selection, try again.");
                }
            } while (selection != "Q");
            
            
            
        }
        static List<InventoryItem> AddItem(List<InventoryItem> inventoryList)
        {
            Console.WriteLine("1 - Bat");
            Console.WriteLine("2 - Baseball");
            Console.WriteLine("3 - Batting Glove");
            Console.WriteLine("4 - Glove");
            Console.WriteLine("5 - Catcher Gear");
            Console.Write("Enter in the category number to add or any other value to quit: ");
            string addItem = Console.ReadLine();
            
            if (addItem != "1" &&
                addItem != "2" &&
                addItem != "3" &&
                addItem != "4" &&
                addItem != "5")  
            {
                return inventoryList;
            } 
            else
            {
                Console.Write("Enter in the Manufactuer: ");
                string newManufacturer = Console.ReadLine();
                Console.Write("Enter in the Price: ");
                decimal newPrice = Convert.ToDecimal(Console.ReadLine());
                Console.Write("Enter in the Quantity: ");
                int newQty = Convert.ToInt32(Console.ReadLine());
                int lastItem = 0;
                foreach (InventoryItem item in inventoryList)
                {
                    if(Convert.ToInt32(item.InventoryNum) >= lastItem)
                    {
                        lastItem = Convert.ToInt32(item.InventoryNum) + 1;
                    }
                }
                if (addItem == "1")
                {
                    Console.Write("Enter in bat Name: ");
                    string newName = Console.ReadLine();
                    Console.Write("Enter in the Length: ");
                    int newLength = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter in the Weight: ");
                    int newWeight = Convert.ToInt32(Console.ReadLine());
                    inventoryList.Add(new Bat(Convert.ToString(lastItem), "Bat", newManufacturer, newPrice, newQty, 
                        newName, newLength, newWeight));

                    string outputFileLoc = @"C:\Users\cn220029\directorysearch\baseballstore.txt";
                    using (StreamWriter outputFile = new StreamWriter(outputFileLoc, true))
                    {
                        outputFile.WriteLine(inventoryList[lastItem - 1].InventoryNum + "|" +
                            inventoryList[lastItem - 1].Item + "|" +
                            inventoryList[lastItem - 1].Manufacturer + "|" +
                            inventoryList[lastItem - 1].Price + "|" +
                            inventoryList[lastItem - 1].Quantity + "|" +
                            newName + "|" +
                            newLength + "|" +
                            newWeight);
                    }
                }
                else
                if (addItem == "2")
                {
                    Console.Write("Enter in skin type: ");
                    string newSkin = Console.ReadLine();
                    Console.Write("Enter in the stitch height: ");
                    int newStitch = Convert.ToInt32(Console.ReadLine());
                    inventoryList.Add(new Baseball(Convert.ToString(lastItem), "Baseball", newManufacturer, newPrice, newQty,
                                    newSkin, newStitch));
                }
                else
                if (addItem == "3")
                {
                    Console.Write("Enter in size: ");
                    string newSize = Console.ReadLine();
                    Console.Write("Enter in the durability: ");
                    string newDurability = Console.ReadLine();
                    inventoryList.Add(new BattingGlove(Convert.ToString(lastItem), "Batting Glove", newManufacturer, newPrice, newQty,
                                    newSize, newDurability));
                } 
                else
                if (addItem == "4")
                {
                    Console.Write("Enter in glove type: ");
                    string newType = Console.ReadLine();
                    Console.Write("Enter in color: ");
                    string newColor = Console.ReadLine();
                    inventoryList.Add(new Glove(Convert.ToString(lastItem), "Glove", newManufacturer, newPrice, newQty,
                                    newType, newColor));
                } 
                else
                if (addItem == "5")
                {
                    Console.Write("Enter in size: ");
                    string newSize = Console.ReadLine();
                    Console.Write("Enter in the color: ");
                    int newColor = Convert.ToInt32(Console.ReadLine());
                    inventoryList.Add(new Baseball(Convert.ToString(lastItem), "Catcher Gear", newManufacturer, newPrice, newQty,
                                    newSize, newColor));
                }
               
            }
            Console.WriteLine("Item successfully added to inventory");
            return inventoryList;
        }
        static void SearchByPrice(List<InventoryItem> inventoryList)
        {
            Console.Write("Enter minimum price: ");
            decimal minPriceSearch = Convert.ToDecimal(Console.ReadLine());
            Console.Write("Enter maximum price: ");
            decimal maxPriceSearch = Convert.ToDecimal(Console.ReadLine());

            var categoryQuery = from c in inventoryList
                                where c.Price >= minPriceSearch &&
                                c.Price <= maxPriceSearch
                                orderby c.Price ascending
                                select c;
            Console.WriteLine("--- Products ranging between {0} and {1} ---", minPriceSearch, maxPriceSearch);
            foreach (InventoryItem item in categoryQuery)
            {
                item.DisplayAllInventory();
            }
        }
        static void SearchByManu(List<InventoryItem> inventoryList)
        {
            Console.Write("Enter in the Manufacturer to search by: ");
            string manufacturerSearch = Console.ReadLine();

            var categoryQuery = from c in inventoryList
                                where c.Manufacturer == manufacturerSearch
                                orderby c.Price ascending
                                select c;
            Console.WriteLine("--- Products matching {0} ---", manufacturerSearch);
            foreach (InventoryItem item in categoryQuery)
            {
                item.DisplayAllInventory();
            }
        }
        static void SearchByCategory(List<InventoryItem> inventoryList)
        {
            Console.Write("Enter in the category to search by: ");
            string categorySearch = Console.ReadLine();

            var categoryQuery = from c in inventoryList
                                where c.Item == categorySearch
                                orderby c.Price ascending
                                select c;
            Console.WriteLine("--- Products matching {0} ---", categorySearch);
            foreach (InventoryItem item in categoryQuery)
            {
                item.DisplayAllInventory();
            }
        }
        static void ShowAll(List<InventoryItem> inventoryList )
        {
            string getDetails = null;
            do
            {
                Console.WriteLine("");
                Console.WriteLine($"ID{"Category",15}{"Manufacturer",20}");
                foreach (InventoryItem item in inventoryList)
                {
                    item.DisplayAllInventory();
                }

                Console.Write("Enter in the Item ID to get Details (Q to Quit): ");
                getDetails = Console.ReadLine();

                bool foundIt = inventoryList.Exists(x => x.InventoryNum == getDetails);
                if (foundIt == true)
                {
                    ShowItemDetail(getDetails, inventoryList);
                }
                else
                if (foundIt == false && getDetails != "Q")
                {
                    Console.WriteLine("Inventory Item not Found, try again.");
                }
            } while (getDetails != "Q");
        }
        static void ShowItemDetail(string getDetails, List<InventoryItem> inventoryListInput)
        {
            InventoryItem inventoryDetail = null;
            List<InventoryItem> inventoryList = inventoryListInput;
            inventoryDetail = inventoryList.Find(x => x.InventoryNum == getDetails);
            inventoryDetail.Display();
            Console.Write("Enter P to Purchase, any other value to return: ");
            string action = Console.ReadLine();

            if (action != "P")
            {
                return;
            }
            Console.Write("Enter Your Name: ");
            string name = Console.ReadLine();
            Console.Write("Enter Your Street Address: ");
            string streetAddress = Console.ReadLine();

            GenerateInvoice(inventoryDetail, name, streetAddress);

            return;
        }
        static void GenerateInvoice(InventoryItem inventoryDetail, string name, string streetAddress)
        {
            string outputFileLoc = @"C:\Users\cn220029\directorysearch\baseballInvoice.txt";
            //using (StreamWriter outputFile = new StreamWriter(outputFileLoc, true)) True is for append
            using (StreamWriter outputFile = new StreamWriter(outputFileLoc))
            {
                inventoryDetail.CreateInvoice(outputFile, name, streetAddress);
            }
            Console.WriteLine("Congratulations on your new " + inventoryDetail.Item + "!");
            Console.WriteLine("Your order will arrive in 2 business days!");
            return;
        }
        static List<InventoryItem> LoadData()
        {
            List<InventoryItem> inventoryList = new List<InventoryItem>();
            string fileName = @"C:\Users\cn220029\directorysearch\baseballstore.txt";
            Console.WriteLine("Trying to process file: " + fileName);

            StreamReader inputFile = null;
            try
            {
                // process file
                using (inputFile = new StreamReader(fileName))
                {
                    while (inputFile.EndOfStream != true)
                    {
                        string text = inputFile.ReadLine();
                        string[] spliced = text.Split("|");
                        if (spliced[1] == "Bat")
                        {
                            inventoryList.Add(new Bat(spliced[0], spliced[1],
                                spliced[2], Convert.ToDecimal(spliced[3]),
                                Convert.ToInt32(spliced[4]), spliced[5],
                                Convert.ToInt32(spliced[6]), Convert.ToInt32(spliced[7])));
                        }
                        else
                        if (spliced[1] == "Baseball")
                        {
                            inventoryList.Add(new Baseball(spliced[0], spliced[1],
                                spliced[2], Convert.ToDecimal(spliced[3]),
                                Convert.ToInt32(spliced[4]), spliced[5],
                                Convert.ToInt32(spliced[6])));
                        }
                        else
                        if (spliced[1] == "Glove")
                        {
                            inventoryList.Add(new Glove(spliced[0], spliced[1],
                                spliced[2], Convert.ToDecimal(spliced[3]),
                                Convert.ToInt32(spliced[4]), spliced[5],
                                spliced[6]));
                        }
                        if (spliced[1] == "Catcher Gear")
                        {
                            inventoryList.Add(new CatcherGear(spliced[0], spliced[1],
                                spliced[2], Convert.ToDecimal(spliced[3]),
                                Convert.ToInt32(spliced[4]), spliced[5],
                                spliced[6]));
                        }
                        if (spliced[1] == "Batting Glove")
                        {
                            inventoryList.Add(new BattingGlove(spliced[0], spliced[1],
                                spliced[2], Convert.ToDecimal(spliced[3]),
                                Convert.ToInt32(spliced[4]), spliced[5],
                                spliced[6]));
                        }
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }
            return inventoryList;
        }
    }
}
