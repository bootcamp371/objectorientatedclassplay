﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballShop
{
    class Baseball : InventoryItem
    {
        public string SkinType { get; set; }
        public int StitchHeight { get; set; }

        public Baseball(string inventoryItem, string item, string manufacturer, decimal price, int quantity,
               string skinType, int stitchHeight) : base(inventoryItem, item, manufacturer, price, quantity)
        {
            this.SkinType = skinType;
            this.StitchHeight = stitchHeight;
            
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine($"{"Skin Type:", 13} " + this.SkinType);
            Console.WriteLine($"{"Stitch Hght:",13} " + this.StitchHeight);
        }

        public override void CreateInvoice(StreamWriter outputFile, string name, string streetAddress)
        {
            base.CreateInvoice(outputFile, name, streetAddress);
            outputFile.WriteLine($"{"Skin Type:",13} " + this.SkinType);
            outputFile.WriteLine($"{"Stitch Hght:",13} " + this.StitchHeight);
        }
    }
}
