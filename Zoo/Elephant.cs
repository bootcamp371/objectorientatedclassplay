﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Elephant : Animal, IHerbivore
    {
        public Elephant(string Name, int Age): base(Name, Age)
        {
        }
        public override string MakeSound()
        {
            return "Trumpet";
        }

        public string Graze()
        {
            return "The elephant uses its trunk to pull leaves and branches from trees.";
        }
    }
}
