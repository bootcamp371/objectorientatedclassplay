﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Lion: Animal, ICarnivore
    {

        public Lion(string Name, int Age) : base(Name, Age)
        {

        }

        public override string MakeSound()
        {
            return "Roar";
        }

        public string Hunt()
        {
            return "The lion stalks its prey and pounces to catch it";
        }
    }
}
