﻿using System;

namespace Zoo
{
    class Program
    {
        static void Main(string[] args)
        {

            Lion Leo = new Lion("Leo", 5);
            Leo.DisplayInfo();

            Elephant dumbo = new Elephant("Dumbo", 15);
            dumbo.DisplayInfo();

            Giraffe longneck = new Giraffe("LongNeck", 8);
            longneck.DisplayInfo();

            Animal[] animals = new Animal[6];

            animals[0] = new Lion("Lio", 5);
            animals[1] = new Lion("Simba", 2);
            animals[2] = new Elephant("Dumbo", 12);
            animals[3] = new Elephant("Tubby", 15);
            animals[4] = new Lion("Melman", 9);
            animals[5] = new Giraffe("LongNeck", 10);


            foreach (Animal singleAnimal in animals)
            {
                singleAnimal.DisplayInfo();
                if (singleAnimal is ICarnivore)
                {
                    //Need to cast to a type Carnivore (inline version)
                    Console.WriteLine(((ICarnivore)singleAnimal).Hunt());
                }
                else if (singleAnimal is IHerbivore)
                {
                    //Need to cast to a type Herbivore (cast then call)
                    IHerbivore ani = (IHerbivore)singleAnimal;
                    Console.WriteLine(ani.Graze());
                    
                }
                
            }
            
        }
    }
}
