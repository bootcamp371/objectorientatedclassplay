﻿using System;
using System.IO;

namespace FileProcessing1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            for (int i = 1; i <= 3; i++)
            {
                //StreamReader sr = null;
                string fileName = @"C:\Users\cn220029\directorysearch\PayrollData" + Convert.ToString(i) + ".txt";
                Console.WriteLine("Trying to process file: " + fileName);
                StreamReader inputFile = null;
                try
                {
                    // process file
                    using (inputFile = new StreamReader(fileName))
                    {
                        while (inputFile.EndOfStream != true)
                        {
                            string text = inputFile.ReadLine();
                            string[] spliced = text.Split("|");
                            if (spliced.Length != 3)
                            {
                                Console.WriteLine("Error in file");
                            }
                            else
                            {
                                TimeCard timeCard = TimeCard.CreateTimeCard(text);
                                Console.WriteLine("Name: " + timeCard.Name);
                                Console.WriteLine("Hour Worked: " + timeCard.HoursWorked);
                                Console.WriteLine("Pay Rate:" + timeCard.PayRate);
                                Console.WriteLine("Total Pay: " + timeCard.GetGrossPay());
                            }
                        }
                    }
                }
                catch (FileNotFoundException ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
                finally
                {
                    if (inputFile != null) inputFile.Close();
                }
            }
        }
    }
}
