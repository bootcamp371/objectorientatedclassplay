﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileProcessing1
{
    class TimeCard
    {
        public string Name { get; set; }
        public decimal HoursWorked { get; set; }
        public decimal PayRate { get; set; }

        public static TimeCard CreateTimeCard(string inputData)
        {
            TimeCard timeCard = new TimeCard();
            string[] spliced = inputData.Split("|");
            timeCard.Name = spliced[0];
            timeCard.HoursWorked = Convert.ToDecimal(spliced[1]);
            timeCard.PayRate = Convert.ToDecimal(spliced[2]);

            return timeCard;
        }

        public decimal GetGrossPay()
        {
            decimal totalPay;

            if (this.HoursWorked <= 40)
            {
                totalPay = HoursWorked * this.PayRate;
            } else
            {
                totalPay = (40 * this.PayRate) + (HoursWorked - 40) * (this.PayRate * 1.5m);
            }

            return totalPay;
        }
    }
}
