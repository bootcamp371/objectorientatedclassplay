﻿using System;

namespace AssetManager
{
    class Program
    {
        static void Main(string[] args)
        {

            //Asset trial = new Asset();

            Asset[] assetList = new Asset[6];

            assetList[0] = new Stock("01", "Stock", "2015", 7, "F", 12.95M, 100);
            assetList[1] = new Stock("02", "Stock", "2018", 22.75M, "INTC", 29.95M, 200);
            assetList[2] = new Stock("03", "Stock", "2017", 17.95M, "KMI", 16.45M, 1000);

            assetList[3] = new Car("04", "Car", "2016", 85000M, "Expedition", "2016", 79858);
            assetList[4] = new Car("05", "Car", "2021", 39000M, "Cherokee", "2021", 8985);
            assetList[5] = new Car("06", "Car", "2023", 50000M, "Camaro", "2022", 50);


            //this way would work too
            foreach (Asset asset in assetList)
            {
                asset.DisplayInfo();
                Console.WriteLine("-----------------------");
            }
            
            for (int i = 0; i < assetList.Length; i++)
            {
                assetList[i].DisplayInfo();
                Console.WriteLine("-----------------------");
            }

            for (int i = 0; i < assetList.Length; i++)
            {
                if (assetList[i] is Stock)
                {
                    Stock wStock = (Stock)assetList[i];
                    wStock.DisplayStock();
                }
               
                if (assetList[i] is Car)
                {
                    Car wCar = (Car)assetList[i];
                    wCar.DisplayCar();
                }
                
            }






        }
    }
}
