﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Stock : Asset
    {
        public string StockTicker { get; set; }
        public decimal CurrentStockPrice { get; set; }
        public decimal NumberOfShares { get; set; }

        public Stock(string AssetId, string Description, string DateAquired, decimal OriginalCost, 
                     string StockTicker, decimal CurrentStockPrice, decimal NumberOfShares) :
                   base(AssetId, Description, DateAquired, OriginalCost)
        {
            this.StockTicker = StockTicker;
            this.CurrentStockPrice = CurrentStockPrice;
            this.NumberOfShares = NumberOfShares;
        }

        public override decimal GetValue()
        {
            return (decimal) this.CurrentStockPrice * (decimal) this.NumberOfShares;
        }

        public void DisplayStock()
        {
            DisplayInfo();

            Console.WriteLine("Stock Ticker: " + this.StockTicker);
            Console.WriteLine("Current Stock Price: " + this.CurrentStockPrice);
            Console.WriteLine("Number of Shares: " + this.NumberOfShares);
            Console.WriteLine("-----------------------");

        }
    }


}
