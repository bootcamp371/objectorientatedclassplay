﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Car : Asset
    {

        public string Model { get; set; }
        public string Year { get; set; }
        public int OdometerReading { get; set; }

        public Car(string AssetId, string Description, string DateAquired, decimal OriginalCost,
                   string Model, string Year, int OdometerReading) :
                   base(AssetId, Description, DateAquired, OriginalCost)
        {
            this.Model = Model;
            this.Year = Year;
            this.OdometerReading = OdometerReading;
            
        }

        public override decimal GetValue()
        {
            int year = 2023;
            int yearsOld = year - Convert.ToInt32(this.Year);  

            if (yearsOld < 7)
            {
                return 
                    OriginalCost * (decimal)(1 - (.02 * yearsOld));
            } else
            {
                if (this.OdometerReading > 100000)
                {
                    return OriginalCost * (decimal).3;
                } else
                {
                    return OriginalCost * (decimal).1;
                }
            }
        }

        public void DisplayCar()
        {
            DisplayInfo();
            Console.WriteLine("Year: " + this.Year);
            Console.WriteLine("Model: " + this.Model);
            Console.WriteLine("Miles: " + this.OdometerReading);
            Console.WriteLine("-----------------------");
        }


    }
}
