﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Asset
    {

        public string AssetId { get; set; }
        public string Description { get; set; }
        public string DateAquired { get; set; }
        public decimal OriginalCost { get; set; }
        
        public Asset(string AssetId, string Description, string DateAquired, decimal OriginalCost)
        {
            this.AssetId = AssetId;
            this.Description = Description;
            this.DateAquired = DateAquired;
            this.OriginalCost = OriginalCost;
        }

        public virtual decimal GetValue()
        {
            return this.OriginalCost;
        }

        public virtual void DisplayInfo()
        {
            Console.WriteLine("Description: " + this.Description);
            Console.WriteLine("Date Aquired: " + this.DateAquired);
            Console.WriteLine("Original Cost: " + this.OriginalCost);
            Console.WriteLine("Current Value: " + this.GetValue());
            
        }
    }
}
