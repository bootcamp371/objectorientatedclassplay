﻿using System;

namespace ClassPlay
{
    class Program
    {
        static void Main(string[] args)
        {

            Employee employee = new Employee("Mark", "Smith", "456", "2018", "Janitor", "Maint", 40000);

            Employee employee1 = new Employee("Mark", "Davis", "789");
            /*employee.FirstName = "Mark";
            employee.LastName = "Galasso";
            employee.EmployeeId = "123";
            employee.HireYear = "2017";
            employee.JobTitle = "Janitorr";
            employee.Department = "Maint";
            employee.Salary = 37000;*/
            DisplayEmployee(employee);
            DisplayEmployee(employee1);

            Employee employeeChain = new Employee("Joe", "Smith", "579");
            DisplayEmployee(employeeChain);

            // Have to have a constructor that does not have inputs
            Employee employeeInitializer = new Employee {
                FirstName = "Jane", LastName = "Doe", EmployeeId = "975",
                HireYear = "1999", JobTitle = "Home Maker", Department = "House", Salary = 10000};
            DisplayEmployee(employeeInitializer);

            //need to have an instance to use.
            employee.Display();
            //Everyone can see this guy but silly to use since you need an instance to work
            Employee.StaticDisplay(employee); 


            employee.Promote("VP of Awesomeness", 1000000);
            employee.Display();
        }

        public static void DisplayEmployee(Employee employee)
        {
            Console.WriteLine("Employee First Name: " + employee.FirstName);
            Console.WriteLine("Employee Last Name: " + employee.LastName);
            Console.WriteLine("Employee Id: " + employee.EmployeeId);
            Console.WriteLine("Employee hire year: " + employee.HireYear);
            Console.WriteLine("Employee Job Title: " + employee.JobTitle);
            Console.WriteLine("Employee Department: " + employee.Department);
            Console.WriteLine("Employee Salary: " + employee.Salary);
        }
    }
}
