﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPlay
{
    class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeId { get; set; }
        public string HireYear { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public decimal Salary { get; set; }

        public Employee()
        {

        }
        public Employee(string firstName,
            string lastName,
            string employeeId,
            string hireYear,
            string jobTitle,
            string department,
            decimal salary)
        {
            FirstName = firstName;
            LastName = lastName;
            EmployeeId = employeeId;
            HireYear = hireYear;
            JobTitle = jobTitle;
            Department = department;
            Salary = salary;
        }

        public Employee(string firstName, string lastName, string employeeId) : 
            this(firstName, lastName, employeeId, "2023", "New Hire", "TBD", 31200)
        {
            
        }

        public void Display()
        {
            Console.WriteLine("Object Instance");
            Console.WriteLine("Employee First Name: " + this.FirstName);
            Console.WriteLine("Employee Last Name: " + this.LastName);
            Console.WriteLine("Employee Id: " + this.EmployeeId);
            Console.WriteLine("Employee hire year: " + this.HireYear);
            Console.WriteLine("Employee Job Title: " + this.JobTitle);
            Console.WriteLine("Employee Department: " + this.Department);
            Console.WriteLine("Employee Salary: " + this.Salary);

        }

        public static void StaticDisplay(Employee employee)
        {
            Console.WriteLine("Static Version");
            Console.WriteLine("Employee First Name: " + employee.FirstName);
            Console.WriteLine("Employee Last Name: " + employee.LastName);
            Console.WriteLine("Employee Id: " + employee.EmployeeId);
            Console.WriteLine("Employee hire year: " + employee.HireYear);
            Console.WriteLine("Employee Job Title: " + employee.JobTitle);
            Console.WriteLine("Employee Department: " + employee.Department);
            Console.WriteLine("Employee Salary: " + employee.Salary);

        }

        public void Promote(string newTitle, decimal newPay)
        {
            this.JobTitle = newTitle;
            this.Salary = newPay;
        }
        /*public Employee(string firstName,
            string lastName,
            string employeeId
            )
        {
            DateTime now = DateTime.Now;
            FirstName = firstName;
            LastName = lastName;
            EmployeeId = employeeId;
            HireYear = now.ToString("yyyy");
            JobTitle = "New Hire";
            Department = "TBD";
            Salary = 31200;

        }*/

    }
}
