﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ToDoTracker
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ToDoTask> todos = new List<ToDoTask>();
           
            todos.Add(new ToDoTask("D1", "Do the disahes", 5, "Sandy", new DateTime(2023, 5 , 15)));
            todos.Add(new ToDoTask("B1", "Make the beds", 1, "James", new DateTime(2023, 6 , 5)));
            todos.Add(new ToDoTask("T1", "Clean the Toilets", 4, "James", new DateTime(2023, 3, 5)));
            todos.Add(new ToDoTask("S5", "Smoke Cigars", 3, "Mark", new DateTime(2023, 3, 23)));
            todos.Add(new ToDoTask("V3", "Vacuum ", 2, "Anthony", new DateTime(2023, 4, 1)));
            todos.Add(new ToDoTask("M3", "Mow the Lawn", 5, "Anthony", new DateTime(2020, 5, 31)));


            int answer = 0;
            do
            {
                Console.WriteLine("1: Add a Task");
                Console.WriteLine("2: Remove a Task");
                Console.WriteLine("3: List Tasks by Due Date");
                Console.WriteLine("4: List Tasks by Difficulty ");
                Console.WriteLine("5: Quit");
                Console.Write("Enter your selection: ");
                answer = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Your Selection was : "  + answer);
                if (answer == 1)
                {
                    Console.Write("Enter Task Id: ");
                    string newTaskId = Console.ReadLine();
                    Console.Write("Enter Task Description: ");
                    string newDescription = Console.ReadLine();
                    Console.Write("Enter Difficulty (1-5): ");
                    int newDifficulty = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter Assigned To: ");
                    string newAssignedTo = Console.ReadLine();
                    Console.Write("Enter Due Date: ");
                    DateTime newDueDate = Convert.ToDateTime(Console.ReadLine());
                    todos.Add(new ToDoTask(newTaskId, newDescription, newDifficulty, newAssignedTo, newDueDate));
                    Console.WriteLine("Task Added!");
                } else
                if (answer == 2)
                {
                    foreach(ToDoTask todo in todos)
                    {
                        todo.Display();
                    }
                    Console.WriteLine("Enter the Task ID to remove: ");
                    string deleteMe = Console.ReadLine();
                    ToDoTask matching = todos.SingleOrDefault(c => c.TaskId == deleteMe);
                    if (matching != null)
                    {
                        todos.Remove(matching);
                        Console.WriteLine(deleteMe + " was removed!");
                    }
                    else
                    {
                        Console.WriteLine("Invalid selection, try again.");
                    }
                } else
                if (answer == 3)
                {
                    todos.Sort(new AscendingDueDateSorter());
                    foreach (ToDoTask todo in todos)
                    {
                        todo.Display();
                    }
                } else
                if (answer == 4)
                {
                  
                    todos.Sort(new AscendingDifficultySorter());
                    foreach (ToDoTask todo in todos)
                    {
                        todo.Display();
                    }
                } 
                else
                {
                    if (answer != 5)
                    {
                        Console.WriteLine("Invalid Entry, Try Again");
                    }
                }
            } while (answer != 5) ;
        }
    }
}
