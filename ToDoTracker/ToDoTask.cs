﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoTracker
{
    public class ToDoTask
    {
        public string TaskId { get; set; }
        public string Description { get; set; }
        public int Difficulty { get; set; }
        public string AssignedTo { get; set; }
        public DateTime DueDate { get; set; }

        public ToDoTask(string newTaskId, string newDescription, int newDifficulty, 
                        string newAssignedTo, DateTime newDueDate)
        {
            this.TaskId = newTaskId;
            this.Description = newDescription;
            this.Difficulty = newDifficulty;
            this.AssignedTo = newAssignedTo;
            this.DueDate = newDueDate;
        }
        public void Display()
        {
            Console.WriteLine(this.TaskId + " - " +
                                this.Description + " Difficulty: " +
                                this.Difficulty + " Assigned to: " +
                                this.AssignedTo + " Due Date " +
                                this.DueDate);
        }
    }
}
