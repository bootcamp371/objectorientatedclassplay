﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoTracker
{
    public class AscendingDifficultySorter : IComparer<ToDoTask>
    {
        public int Compare(ToDoTask x, ToDoTask y)
        {
            return x.Difficulty - y.Difficulty;
        }
    }

    public class AscendingDueDateSorter : IComparer<ToDoTask>
    {
        public int Compare(ToDoTask x, ToDoTask y)
        {
            return x.DueDate.CompareTo(y.DueDate);
        }
    }
}
