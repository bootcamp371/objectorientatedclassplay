﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Person : IComparable<Person>
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public Person(string name, int age, string city, string state, string zipCode)
        {
            this.Name = name;
            this.Age = age;
            this.City = city;
            this.State = state;
            this.ZipCode = zipCode;
        }

        public void HaveABirthday()
        {
            this.Age = this.Age + 1;
        }

        public void Move(string newCity, string newState, string newZip)
        {
            this.City = newCity;
            this.State = newState;
            this.ZipCode = newZip;
        }

        public virtual void Display()
        {
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("Age: " + Age);
            Console.WriteLine("City: " + City);
            Console.WriteLine("State: " + State);
            Console.WriteLine("Zip: " + ZipCode);
            
           
        }

        public int CompareTo(Person other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}
