﻿using System;
using System.Collections.Generic;

namespace PeopleManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            List<Person> myPeople = new List<Person>();
            myPeople.Add(new Person("Zack", 18, "Bayside", "CA", "90210"));
            myPeople.Add(new Person("James", 12, "Macomb", "MI", "48044"));
            myPeople.Add(new Person("Anthony", 15, "Las Vegas", "NV", "85625"));
            myPeople.Add(new Worker("Mark", 47, "Detroit", "MI", "48011", "Janitor", 45000));
            myPeople.Add(new Worker("Maaike", 31, "Amsterdam", "NT", "84584", "Teacher", 150000));

            foreach (Person peep in myPeople)
            {
                peep.Display();
            }

            myPeople.Sort();
            Console.WriteLine("After Sort");
            Console.WriteLine("--------------------");
            foreach (Person peep in myPeople)
            {
                peep.Display();
            }
            myPeople.Sort(new AscendingAgeSorter());

            Console.WriteLine("After Ascending Age");
            Console.WriteLine("--------------------");
            foreach (Person peep in myPeople)
            {
                peep.Display();
            }

            myPeople.Sort(new AscendingCitySorter());

            Console.WriteLine("After Ascending City");
            Console.WriteLine("--------------------");
            foreach (Person peep in myPeople)
            {
                peep.Display();
            }
            myPeople.Sort(new DescendingCitySorter());

            Console.WriteLine("After Descending City");
            Console.WriteLine("--------------------");
            foreach (Person peep in myPeople)
            {
                peep.Display();
            }

            
        }
    }
}
