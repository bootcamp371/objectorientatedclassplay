﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Worker : Person    
    {

        public string JobTitle { get; set; }
        public int Salary { get; set; }

        public Worker(string name, int age, string city, string state, string zipCode, string jobTitle, int salary):
            base(name, age, city, state, zipCode)
        {
            this.JobTitle = jobTitle;
            this.Salary = salary;
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine("Job Title: " + this.JobTitle);
            Console.WriteLine("Salary: " + this.Salary);
        }
    }
}
