﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{


    public class DescendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.Age - x.Age;
        }
    }
    public class AscendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.Age - y.Age;
        }
    }

    public class DescendingCitySorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.City.CompareTo(x.City);
            
        }
    }

    public class AscendingCitySorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.City.CompareTo(y.City);

        }
    }
}
