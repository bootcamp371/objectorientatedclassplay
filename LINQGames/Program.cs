﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQGames
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<Supplier> suppliers = new List<Supplier> {new Supplier(101, "ACME", "acme.com"),
                                                            new Supplier(201, "Spring Valley", "spring-valley.com")};
            List<Product> products = new List<Product> {new Product(1, "Dark Chocolate Bar", 4.99M, 10, 101),
                                                        new Product(2, "8 oz Guacamole", 5.99M, 27, 201),
                                                        new Product(3, "Milk Chocolate Bar", 3.99M, 16, 101),
                                                        new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 201)};
            // List Suppliers
            Console.WriteLine("--- Supplier List ---");
            var supplierQuery = from p in suppliers
                                orderby p.Name ascending
                                select p;
            foreach (Supplier supplier in supplierQuery)
            {
                Console.WriteLine(supplier.Name);
            }
            var productQuery = from p in products
                                where p.Price <= 5M
                                orderby p.ProductName ascending
                                select p;
            Console.WriteLine("--- Products under $5 ---");
            foreach(Product product in productQuery)
            {
                Console.WriteLine(product.ProductName + " - " + product.Price);
            }
            // Qty of 10 or more onhand
            Console.WriteLine("--- Products with 10 or more on hand ---");
            var qtyGT10Query = from p in products
                                where p.QuantityOnHand >= 10
                                orderby p.QuantityOnHand descending
                                select p;

            foreach (Product product in qtyGT10Query)
            {
                Console.WriteLine(product.ProductName + " - Qty on hand: " + product.QuantityOnHand);
            }

            //Supplier 201 detail
            Console.WriteLine("--- Supplier 201 Detail ---");
            Supplier Supplier201 = (from s in suppliers
                                    where s.SupplierId == 201
                                    select s).FirstOrDefault();
            Console.WriteLine("ID: " + Supplier201.SupplierId + " Name: " + Supplier201.Name + " URL: " + Supplier201.URL);

            // First product more than $5
            Console.WriteLine("--- First Item more than $5 ---");
            Product FirstOver5 = (from p in products
                                    where p.Price > 5
                                    select p).FirstOrDefault();
            Console.WriteLine("Product: " + FirstOver5.ProductName + " Cost: " + FirstOver5.Price);

            // Count under $5
            Console.WriteLine("--- Count of products under $5 ---");
            int under5 = (from p in products
                            where p.Price <= 5
                            select p).Count();
            Console.WriteLine("There are " + under5 + " products under $5");
            //Ready to reorder
            Console.WriteLine("--- Products ready to reorder ---");
            List<Product> readyToReorder = GetItemsToBeReordered(products);
            foreach(Product reorder in readyToReorder)
            {
                Console.WriteLine("Product Description: " + reorder.ProductName + " Qty onhand: " + reorder.QuantityOnHand);
            }

            var joinQuery = from p in products
                            join s in suppliers on p.SupplierId equals s.SupplierId
                            select new
                            {
                                p.ProductName,
                                p.Price,
                                SupplierName = s.Name
                            };
            Console.WriteLine("--- Join !  ---");
            foreach (var p in joinQuery)
            {
                Console.WriteLine("Product Name: " + p.ProductName + " Price: " + p.Price + " Supplier: " + p.SupplierName); ;
            }
        }

        public static List<Product> GetItemsToBeReordered(List<Product> products)
        {
            List<Product> readyToReorder = (from p in products
                                            where p.QuantityOnHand <= 10
                                            orderby p.ProductName ascending
                                            select p).ToList();
            return readyToReorder;
        }
    }
}
